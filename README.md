<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a>

# Quick_SVGs

A repo to keep the SVGs I created and are under [CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/) license which is [approved by FSF](https://directory.fsf.org/wiki/License:CC-BY-4.0). For more but brief information read the [license](#license) section of this file.

## Content
To see the images, you can go to the [svg folder](https://codeberg.org/mehrad/Quick_SVGs/src/branch/master/svg) and click on them one by one. Codeberg will render them in your browser like a normal picture. In future I might create a gallary. :)


## License

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

### Freedoms

You are free to:

- **Share** — copy and redistribute the material in any medium or format
- **Adapt** — remix, transform, and build upon the material for any purpose, even commercially 

### Limitations

- **Attribution** — You must give [appropriate credit](https://wiki.creativecommons.org/License_Versions#Detailed_attribution_comparison_chart), provide a link to the license, and [indicate if changes were made](https://wiki.creativecommons.org/License_Versions#Modifications_and_adaptations_must_be_marked_as_such). You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
- **No additional restrictions** — You may not apply legal terms or [technological measures](https://wiki.creativecommons.org/License_Versions#Application_of_effective_technological_measures_by_users_of_CC-licensed_works_prohibited) that legally restrict others from doing anything the license permits.

